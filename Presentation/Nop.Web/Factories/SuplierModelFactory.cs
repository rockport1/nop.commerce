﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Supliers;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Supliers;
using Nop.Web.Models.Supliers;

namespace Nop.Web.Factories
{
    public class SuplierModelFactory : ISuplierModelFactory
    {
        #region Fields

        private readonly CaptchaSettings _captchaSettings; 
        private readonly CommonSettings _commonSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly ISuplierAttributeParser _SuplierAttributeParser;
        private readonly ISuplierAttributeService _SuplierAttributeService;
        private readonly IWorkContext _workContext;
        private readonly MediaSettings _mediaSettings;
        private readonly SuplierSettings _SuplierSettings;

        #endregion

        #region Ctor

        public SuplierModelFactory(CaptchaSettings captchaSettings,
            CommonSettings commonSettings,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            ISuplierAttributeParser SuplierAttributeParser,
            ISuplierAttributeService SuplierAttributeService,
            IWorkContext workContext,
            MediaSettings mediaSettings,
            SuplierSettings SuplierSettings)
        {
            _captchaSettings = captchaSettings;
            _commonSettings = commonSettings;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _pictureService = pictureService;
            _SuplierAttributeParser = SuplierAttributeParser;
            _SuplierAttributeService = SuplierAttributeService;
            _workContext = workContext;
            _mediaSettings = mediaSettings;
            _SuplierSettings = SuplierSettings;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Prepare Suplier attribute models
        /// </summary>
        /// <param name="SuplierAttributesXml">Suplier attributes in XML format</param>
        /// <returns>List of the Suplier attribute model</returns>
        protected virtual IList<SuplierAttributeModel> PrepareSuplierAttributes(string SuplierAttributesXml)
        {
            var result = new List<SuplierAttributeModel>();

            var SuplierAttributes = _SuplierAttributeService.GetAllSuplierAttributes();
            foreach (var attribute in SuplierAttributes)
            {
                var attributeModel = new SuplierAttributeModel
                {
                    Id = attribute.Id,
                    Name = _localizationService.GetLocalized(attribute, x => x.Name),
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType,
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _SuplierAttributeService.GetSuplierAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var valueModel = new SuplierAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = _localizationService.GetLocalized(attributeValue, x => x.Name),
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(valueModel);
                    }
                }

                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.Checkboxes:
                        {
                            if (!string.IsNullOrEmpty(SuplierAttributesXml))
                            {
                                //clear default selection
                                foreach (var item in attributeModel.Values)
                                    item.IsPreSelected = false;

                                //select new values
                                var selectedValues = _SuplierAttributeParser.ParseSuplierAttributeValues(SuplierAttributesXml);
                                foreach (var attributeValue in selectedValues)
                                    foreach (var item in attributeModel.Values)
                                        if (attributeValue.Id == item.Id)
                                            item.IsPreSelected = true;
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //do nothing
                            //values are already pre-set
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            if (!string.IsNullOrEmpty(SuplierAttributesXml))
                            {
                                var enteredText = _SuplierAttributeParser.ParseValues(SuplierAttributesXml, attribute.Id);
                                if (enteredText.Any())
                                    attributeModel.DefaultValue = enteredText[0];
                            }
                        }
                        break;
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.FileUpload:
                    default:
                        //not supported attribute control types
                        break;
                }

                result.Add(attributeModel);
            }

            return result;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare the apply Suplier model
        /// </summary>
        /// <param name="model">The apply Suplier model</param>
        /// <param name="validateSuplier">Whether to validate that the customer is already a Suplier</param>
        /// <param name="excludeProperties">Whether to exclude populating of model properties from the entity</param>
        /// <param name="SuplierAttributesXml">Suplier attributes in XML format</param>
        /// <returns>The apply Suplier model</returns>
        public virtual ApplySuplierModel PrepareApplySuplierModel(ApplySuplierModel model,
            bool validateSuplier, bool excludeProperties, string SuplierAttributesXml)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //if (validateSuplier && _workContext.CurrentCustomer.SuplierId > 0)
            //{
            //    //already applied for Suplier account
            //    model.DisableFormInput = true;
            //    model.Result = _localizationService.GetResource("Supliers.ApplyAccount.AlreadyApplied");
            //}

            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnApplySuplierPage;
            model.TermsOfServiceEnabled = _SuplierSettings.TermsOfServiceEnabled;
            model.TermsOfServicePopup = _commonSettings.PopupForTermsOfServiceLinks;

            if (!excludeProperties)
            {
                model.Email = _workContext.CurrentCustomer.Email;
            }

            //Suplier attributes
            model.SuplierAttributes = PrepareSuplierAttributes(SuplierAttributesXml);

            return model;
        }

        /// <summary>
        /// Prepare the Suplier info model
        /// </summary>
        /// <param name="model">Suplier info model</param>
        /// <param name="excludeProperties">Whether to exclude populating of model properties from the entity</param>
        /// <param name="overriddenSuplierAttributesXml">Overridden Suplier attributes in XML format; pass null to use SuplierAttributes of Suplier</param>
        /// <returns>Suplier info model</returns>
        public virtual SuplierInfoModel PrepareSuplierInfoModel(SuplierInfoModel model,
            bool excludeProperties, string overriddenSuplierAttributesXml = "")
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            var Suplier = _workContext.CurrentSuplier;
            if (!excludeProperties)
            {
                model.Description = Suplier.Description;
                model.Email = Suplier.Email;
                model.Name = Suplier.Name;
            }

            var picture = _pictureService.GetPictureById(Suplier.PictureId);
            var pictureSize = _mediaSettings.AvatarPictureSize;
            model.PictureUrl = picture != null ? _pictureService.GetPictureUrl(ref picture, pictureSize) : string.Empty;

            //Suplier attributes
            if (string.IsNullOrEmpty(overriddenSuplierAttributesXml))
                overriddenSuplierAttributesXml = _genericAttributeService.GetAttribute<string>(Suplier, NopSuplierDefaults.SuplierAttributes);
            model.SuplierAttributes = PrepareSuplierAttributes(overriddenSuplierAttributesXml);

            return model;
        }

        #endregion
    }
}
