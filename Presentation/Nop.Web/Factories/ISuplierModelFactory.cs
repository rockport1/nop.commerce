﻿using Nop.Web.Models.Supliers;

namespace Nop.Web.Factories
{
    public interface ISuplierModelFactory
    {
        /// <summary> .
        /// Prepare the apply Suplier model
        /// </summary>
        /// <param name="model">The apply Suplier model</param>
        /// <param name="validateSuplier">Whether to validate that the customer is already a Suplier</param>
        /// <param name="excludeProperties">Whether to exclude populating of model properties from the entity</param>
        /// <param name="SuplierAttributesXml">Suplier attributes in XML format</param>
        /// <returns>The apply Suplier model</returns>
        ApplySuplierModel PrepareApplySuplierModel(ApplySuplierModel model, bool validateSuplier, bool excludeProperties, string SuplierAttributesXml);

        /// <summary>
        /// Prepare the Suplier info model
        /// </summary>
        /// <param name="model">Suplier info model</param>
        /// <param name="excludeProperties">Whether to exclude populating of model properties from the entity</param>
        /// <param name="overriddenSuplierAttributesXml">Overridden Suplier attributes in XML format; pass null to use SuplierAttributes of Suplier</param>
        /// <returns>Suplier info model</returns>
        SuplierInfoModel PrepareSuplierInfoModel(SuplierInfoModel model, bool excludeProperties, string overriddenSuplierAttributesXml = "");
    }
}
