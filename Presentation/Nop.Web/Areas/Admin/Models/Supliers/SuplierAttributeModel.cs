﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor attribute model
    /// </summary>
    public partial class SuplierAttributeModel : BaseNopEntityModel, ILocalizedModel<SuplierAttributeLocalizedModel>
    {
        #region Ctor

        public SuplierAttributeModel()
        {
            Locales = new List<SuplierAttributeLocalizedModel>();
            SuplierAttributeValueSearchModel = new SuplierAttributeValueSearchModel();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Fields.IsRequired")]
        public bool IsRequired { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Fields.AttributeControlType")]
        public int AttributeControlTypeId { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Fields.AttributeControlType")]
        public string AttributeControlTypeName { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        public IList<SuplierAttributeLocalizedModel> Locales { get; set; }

        public SuplierAttributeValueSearchModel SuplierAttributeValueSearchModel { get; set; }

        #endregion
    }

    public partial class SuplierAttributeLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Fields.Name")]
        public string Name { get; set; }
    }
}