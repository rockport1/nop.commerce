﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Core.Domain.Catalog;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor model
    /// </summary>
    public partial class SuplierModel : BaseNopEntityModel, ILocalizedModel<SuplierLocalizedModel>
    {
        #region Ctor

        public SuplierModel()
        {
            if (PageSize < 1)
                PageSize = 5;

            Address = new AddressModel();
            SuplierAttributes = new List<SuplierAttributeModel>();
            Locales = new List<SuplierLocalizedModel>();
            AssociatedCustomers = new List<SuplierAssociatedCustomerModel>();
            SuplierNoteSearchModel = new SuplierNoteSearchModel();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Supliers.Fields.Name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("Admin.Supliers.Fields.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.Description")]
        public string Description { get; set; }

        [UIHint("Picture")]
        [NopResourceDisplayName("Admin.Supliers.Fields.Picture")]
        public int PictureId { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.AdminComment")]
        public string AdminComment { get; set; }

        public AddressModel Address { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.Active")]
        public bool Active { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }        

        [NopResourceDisplayName("Admin.Supliers.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.MetaDescription")]
        public string MetaDescription { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.SeName")]
        public string SeName { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.PageSize")]
        public int PageSize { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.AllowCustomersToSelectPageSize")]
        public bool AllowCustomersToSelectPageSize { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        public List<SuplierAttributeModel> SuplierAttributes { get; set; }

        public IList<SuplierLocalizedModel> Locales { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.AssociatedCustomerEmails")]
        public IList<SuplierAssociatedCustomerModel> AssociatedCustomers { get; set; }

        //vendor notes
        [NopResourceDisplayName("Admin.Supliers.SuplierNotes.Fields.Note")]
        public string AddSuplierNoteMessage { get; set; }

        public SuplierNoteSearchModel SuplierNoteSearchModel { get; set; }

        #endregion

        #region Nested classes
        
        public partial class SuplierAttributeModel : BaseNopEntityModel
        {
            public SuplierAttributeModel()
            {
                Values = new List<SuplierAttributeValueModel>();
            }

            public string Name { get; set; }

            public bool IsRequired { get; set; }

            /// <summary>
            /// Default value for textboxes
            /// </summary>
            public string DefaultValue { get; set; }

            public AttributeControlType AttributeControlType { get; set; }

            public IList<SuplierAttributeValueModel> Values { get; set; }
        }

        public partial class SuplierAttributeValueModel : BaseNopEntityModel
        {
            public string Name { get; set; }

            public bool IsPreSelected { get; set; }
        }

        #endregion
    }

    public partial class SuplierLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.MetaDescription")]
        public string MetaDescription { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [NopResourceDisplayName("Admin.Supliers.Fields.SeName")]
        public string SeName { get; set; }
    }
}