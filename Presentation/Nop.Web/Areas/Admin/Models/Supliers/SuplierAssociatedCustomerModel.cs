﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary>
    /// Represents a vendor associated customer model
    /// </summary>
    public partial class SuplierAssociatedCustomerModel : BaseNopEntityModel
    {
        #region Properties

        public string Email { get; set; } 

        #endregion
    }
}