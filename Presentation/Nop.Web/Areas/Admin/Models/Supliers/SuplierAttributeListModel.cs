﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor attribute list model
    /// </summary>
    public partial class SuplierAttributeListModel : BasePagedListModel<SuplierAttributeModel>
    {
    }
}