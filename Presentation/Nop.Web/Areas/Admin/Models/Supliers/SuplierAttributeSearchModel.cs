﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor attribute search model
    /// </summary>
    public partial class SuplierAttributeSearchModel : BaseSearchModel
    {
    }
}