﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor list model
    /// </summary>
    public partial class SuplierListModel : BasePagedListModel<SuplierModel>
    {
    }
}