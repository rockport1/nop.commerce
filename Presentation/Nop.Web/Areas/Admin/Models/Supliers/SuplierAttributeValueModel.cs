﻿using System.Collections.Generic;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a Suplier attribute value model
    /// </summary>
    public partial class SuplierAttributeValueModel : BaseNopEntityModel, ILocalizedModel<SuplierAttributeValueLocalizedModel>
    {
        #region Ctor

        public SuplierAttributeValueModel()
        {
            Locales = new List<SuplierAttributeValueLocalizedModel>();
        }

        #endregion

        #region Properties

        public int SuplierAttributeId { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Values.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Values.Fields.IsPreSelected")]
        public bool IsPreSelected { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Values.Fields.DisplayOrder")]
        public int DisplayOrder {get;set;}

        public IList<SuplierAttributeValueLocalizedModel> Locales { get; set; }

        #endregion
    }

    public partial class SuplierAttributeValueLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Supliers.SuplierAttributes.Values.Fields.Name")]
        public string Name { get; set; }
    }
}