﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor note search model
    /// </summary>
    public partial class SuplierNoteSearchModel : BaseSearchModel
    {
        #region Properties

        public int SuplierId { get; set; }
        
        #endregion
    }
}