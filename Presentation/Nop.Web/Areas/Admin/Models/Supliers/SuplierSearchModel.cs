﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor search model
    /// </summary>
    public partial class SuplierSearchModel : BaseSearchModel
    {
        #region Properties

        [NopResourceDisplayName("Admin.Suplier.List.SearchName")]
        public string SearchName { get; set; }

        [NopResourceDisplayName("Admin.Suplier.List.SearchEmail")]
        public string SearchEmail { get; set; }

        #endregion
    }
}