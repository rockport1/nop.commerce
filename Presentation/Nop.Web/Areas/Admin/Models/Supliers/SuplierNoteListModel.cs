﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor note list model
    /// </summary>
    public partial class SuplierNoteListModel : BasePagedListModel<SuplierNoteModel>
    {
    }
}