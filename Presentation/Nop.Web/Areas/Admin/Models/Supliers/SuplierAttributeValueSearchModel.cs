﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor attribute value search model
    /// </summary>
    public partial class SuplierAttributeValueSearchModel : BaseSearchModel
    {
        #region Properties

        public int SuplierAttributeId { get; set; }

        #endregion
    }
}