﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.Supliers
{
    /// <summary> .
    /// Represents a vendor attribute value list model
    /// </summary>
    public partial class SuplierAttributeValueListModel : BasePagedListModel<SuplierAttributeValueModel>
    {
    }
}