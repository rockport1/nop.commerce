﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using Nop.Web.Areas.Admin.Models.Supliers;


namespace Nop.Web.Areas.Admin.Models.Settings
{
    public class SuplierSettingsModel
    {
        #region Ctor

        public SuplierSettingsModel()
        {
            SuplierAttributeSearchModel = new SuplierAttributeSearchModel();
        }

        #endregion

        #region Properties

        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.SupliersBlockItemsToDisplay")]
        public int SupliersBlockItemsToDisplay { get; set; }
        public bool SupliersBlockItemsToDisplay_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.ShowSuplierOnProductDetailsPage")]
        public bool ShowSuplierOnProductDetailsPage { get; set; }
        public bool ShowSuplierOnProductDetailsPage_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.AllowCustomersToContactSupliers")]
        public bool AllowCustomersToContactSupliers { get; set; }
        public bool AllowCustomersToContactSupliers_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.AllowCustomersToApplyForSuplierAccount")]
        public bool AllowCustomersToApplyForSuplierAccount { get; set; }
        public bool AllowCustomersToApplyForSuplierAccount_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.TermsOfServiceEnabled")]
        public bool TermsOfServiceEnabled { get; set; }
        public bool TermsOfServiceEnabled_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.AllowSearchBySuplier")]
        public bool AllowSearchBySuplier { get; set; }
        public bool AllowSearchBySuplier_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.AllowSupliersToEditInfo")]
        public bool AllowSupliersToEditInfo { get; set; }
        public bool AllowSupliersToEditInfo_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.NotifyStoreOwnerAboutSuplierInformationChange")]
        public bool NotifyStoreOwnerAboutSuplierInformationChange { get; set; }
        public bool NotifyStoreOwnerAboutSuplierInformationChange_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.MaximumProductNumber")]
        public int MaximumProductNumber { get; set; }
        public bool MaximumProductNumber_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.AllowSupliersToImportProducts")]
        public bool AllowSupliersToImportProducts { get; set; }
        public bool AllowSupliersToImportProducts_OverrideForStore { get; set; }

        [NopResourceDisplayName("Admin.Configuration.Settings.Suplier.ShowSuplierOnOrderDetailsPage")]
        public bool ShowSuplierOnOrderDetailsPage { get; set; }
        public bool ShowSuplierOnOrderDetailsPage_OverrideForStore { get; set; }

        public SuplierAttributeSearchModel SuplierAttributeSearchModel { get; set; }

        #endregion
    }
}
