﻿using FluentValidation;
using Nop.Core.Domain.Supliers;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.Supliers;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.Supliers
{
    public partial class SuplierAttributeValidator : BaseNopValidator<SuplierAttributeModel>
    {
        public SuplierAttributeValidator(ILocalizationService localizationService, INopDataProvider dataProvider)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Supliers.SuplierAttributes.Fields.Name.Required"));

            SetDatabaseValidationRules<SuplierAttribute>(dataProvider); 
        }
    }
}