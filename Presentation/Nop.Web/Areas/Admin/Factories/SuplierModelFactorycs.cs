﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Supliers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Seo;
using Nop.Services.Supliers;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Common;
using Nop.Web.Areas.Admin.Models.Supliers;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;
namespace Nop.Web.Areas.Admin.Factories
{
    public class SuplierModelFactory : ISuplierModelFactory
    {
        #region Fields

        private readonly IAddressAttributeModelFactory _addressAttributeModelFactory;
        private readonly IAddressService _addressService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ICustomerService _customerService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ISuplierAttributeParser _SuplierAttributeParser;
        private readonly ISuplierAttributeService _SuplierAttributeService;
        private readonly ISuplierService _SuplierService;
        private readonly SuplierSettings _SuplierSettings;

        #endregion

        #region Ctor

        public SuplierModelFactory(IAddressAttributeModelFactory addressAttributeModelFactory,
            IAddressService addressService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ICustomerService customerService,
            IDateTimeHelper dateTimeHelper,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            ILocalizedModelFactory localizedModelFactory,
            IUrlRecordService urlRecordService,
            ISuplierAttributeParser SuplierAttributeParser,
            ISuplierAttributeService SuplierAttributeService,
            ISuplierService SuplierService,
            SuplierSettings SuplierSettings)
        {
            _addressAttributeModelFactory = addressAttributeModelFactory;
            _addressService = addressService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _customerService = customerService;
            _dateTimeHelper = dateTimeHelper;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _localizedModelFactory = localizedModelFactory;
            _urlRecordService = urlRecordService;
            _SuplierAttributeParser = SuplierAttributeParser;
            _SuplierAttributeService = SuplierAttributeService;
            _SuplierService = SuplierService;
            _SuplierSettings = SuplierSettings;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Prepare Suplier associated customer models
        /// </summary>
        /// <param name="models">List of Suplier associated customer models</param>
        /// <param name="Suplier">Suplier</param>
        protected virtual void PrepareAssociatedCustomerModels(IList<SuplierAssociatedCustomerModel> models, Suplier Suplier)
        {
            if (models == null)
                throw new ArgumentNullException(nameof(models));

            if (Suplier == null)
                throw new ArgumentNullException(nameof(Suplier));

            var associatedCustomers = _customerService.GetAllCustomers(SuplierId: Suplier.Id);
            foreach (var customer in associatedCustomers)
            {
                models.Add(new SuplierAssociatedCustomerModel
                {
                    Id = customer.Id,
                    Email = customer.Email
                });
            }
        }

        /// <summary>
        /// Prepare Suplier attribute models
        /// </summary>
        /// <param name="models">List of Suplier attribute models</param>
        /// <param name="Suplier">Suplier</param>
        protected virtual void PrepareSuplierAttributeModels(IList<SuplierModel.SuplierAttributeModel> models, Suplier Suplier)
        {
            if (models == null)
                throw new ArgumentNullException(nameof(models));

            //get available Suplier attributes
            var SuplierAttributes = _SuplierAttributeService.GetAllSuplierAttributes();
            foreach (var attribute in SuplierAttributes)
            {
                var attributeModel = new SuplierModel.SuplierAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _SuplierAttributeService.GetSuplierAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new SuplierModel.SuplierAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }

                //set already selected attributes
                if (Suplier != null)
                {
                    var selectedSuplierAttributes = _genericAttributeService.GetAttribute<string>(Suplier, NopSuplierDefaults.SuplierAttributes);
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                        case AttributeControlType.RadioList:
                        case AttributeControlType.Checkboxes:
                            {
                                if (!string.IsNullOrEmpty(selectedSuplierAttributes))
                                {
                                    //clear default selection
                                    foreach (var item in attributeModel.Values)
                                        item.IsPreSelected = false;

                                    //select new values
                                    var selectedValues = _SuplierAttributeParser.ParseSuplierAttributeValues(selectedSuplierAttributes);
                                    foreach (var attributeValue in selectedValues)
                                        foreach (var item in attributeModel.Values)
                                            if (attributeValue.Id == item.Id)
                                                item.IsPreSelected = true;
                                }
                            }
                            break;
                        case AttributeControlType.ReadonlyCheckboxes:
                            {
                                //do nothing
                                //values are already pre-set
                            }
                            break;
                        case AttributeControlType.TextBox:
                        case AttributeControlType.MultilineTextbox:
                            {
                                if (!string.IsNullOrEmpty(selectedSuplierAttributes))
                                {
                                    var enteredText = _SuplierAttributeParser.ParseValues(selectedSuplierAttributes, attribute.Id);
                                    if (enteredText.Any())
                                        attributeModel.DefaultValue = enteredText[0];
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                        case AttributeControlType.ColorSquares:
                        case AttributeControlType.ImageSquares:
                        case AttributeControlType.FileUpload:
                        default:
                            //not supported attribute control types
                            break;
                    }
                }

                models.Add(attributeModel);
            }
        }

        /// <summary>
        /// Prepare address model
        /// </summary>
        /// <param name="model">Address model</param>
        /// <param name="address">Address</param>
        protected virtual void PrepareAddressModel(AddressModel model, Address address)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            //set some of address fields as enabled and required
            model.CountryEnabled = true;
            model.StateProvinceEnabled = true;
            model.CountyEnabled = true;
            model.CityEnabled = true;
            model.StreetAddressEnabled = true;
            model.StreetAddress2Enabled = true;
            model.ZipPostalCodeEnabled = true;
            model.PhoneEnabled = true;
            model.FaxEnabled = true;

            //prepare available countries
            _baseAdminModelFactory.PrepareCountries(model.AvailableCountries);

            //prepare available states
            _baseAdminModelFactory.PrepareStatesAndProvinces(model.AvailableStates, model.CountryId);

            //prepare custom address attributes
            _addressAttributeModelFactory.PrepareCustomAddressAttributes(model.CustomAddressAttributes, address);
        }

        /// <summary>
        /// Prepare Suplier note search model
        /// </summary>
        /// <param name="searchModel">Suplier note search model</param>
        /// <param name="Suplier">Suplier</param>
        /// <returns>Suplier note search model</returns>
        protected virtual SuplierNoteSearchModel PrepareSuplierNoteSearchModel(SuplierNoteSearchModel searchModel, Suplier Suplier)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (Suplier == null)
                throw new ArgumentNullException(nameof(Suplier));

            searchModel.SuplierId = Suplier.Id;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare Suplier search model
        /// </summary>
        /// <param name="searchModel">Suplier search model</param>
        /// <returns>Suplier search model</returns>
        public virtual SuplierSearchModel PrepareSuplierSearchModel(SuplierSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged Suplier list model
        /// </summary>
        /// <param name="searchModel">Suplier search model</param>
        /// <returns>Suplier list model</returns>
        public virtual SuplierListModel PrepareSuplierListModel(SuplierSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get Supliers
            var Supliers = _SuplierService.GetAllSupliers(showHidden: true,
                name: searchModel.SearchName,
                email: searchModel.SearchEmail,
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize);

            //prepare list model
            var model = new SuplierListModel().PrepareToGrid(searchModel, Supliers, () =>
            {
                //fill in model values from the entity
                return Supliers.Select(Suplier =>
                {
                    var SuplierModel = Suplier.ToModel<SuplierModel>();
                    SuplierModel.SeName = _urlRecordService.GetSeName(Suplier, 0, true, false);

                    return SuplierModel;
                });
            });

            return model;
        }

        /// <summary>
        /// Prepare Suplier model
        /// </summary>
        /// <param name="model">Suplier model</param>
        /// <param name="Suplier">Suplier</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Suplier model</returns>
        public virtual SuplierModel PrepareSuplierModel(SuplierModel model, Suplier Suplier, bool excludeProperties = false)
        {
            Action<SuplierLocalizedModel, int> localizedModelConfiguration = null;

            if (Suplier != null)
            {
                //fill in model values from the entity
                if (model == null)
                {
                    model = Suplier.ToModel<SuplierModel>();
                    model.SeName = _urlRecordService.GetSeName(Suplier, 0, true, false);
                }

                //define localized model configuration action
                localizedModelConfiguration = (locale, languageId) =>
                {
                    locale.Name = _localizationService.GetLocalized(Suplier, entity => entity.Name, languageId, false, false);
                    locale.Description = _localizationService.GetLocalized(Suplier, entity => entity.Description, languageId, false, false);
                    locale.MetaKeywords = _localizationService.GetLocalized(Suplier, entity => entity.MetaKeywords, languageId, false, false);
                    locale.MetaDescription = _localizationService.GetLocalized(Suplier, entity => entity.MetaDescription, languageId, false, false);
                    locale.MetaTitle = _localizationService.GetLocalized(Suplier, entity => entity.MetaTitle, languageId, false, false);
                    locale.SeName = _urlRecordService.GetSeName(Suplier, languageId, false, false);
                };

                //prepare associated customers
                PrepareAssociatedCustomerModels(model.AssociatedCustomers, Suplier);

                //prepare nested search models
                PrepareSuplierNoteSearchModel(model.SuplierNoteSearchModel, Suplier);
            }

            //set default values for the new model
            if (Suplier == null)
            {
                model.PageSize = 6;
                model.Active = true;
                model.AllowCustomersToSelectPageSize = true;
                model.PageSizeOptions = _SuplierSettings.DefaultSuplierPageSizeOptions;
            }

            //prepare localized models
            if (!excludeProperties)
                model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            //prepare model Suplier attributes
            PrepareSuplierAttributeModels(model.SuplierAttributes, Suplier);

            //prepare address model
            var address = _addressService.GetAddressById(Suplier?.AddressId ?? 0);
            if (!excludeProperties && address != null)
                model.Address = address.ToModel(model.Address);
            PrepareAddressModel(model.Address, address);

            return model;
        }

        /// <summary>
        /// Prepare paged Suplier note list model
        /// </summary>
        /// <param name="searchModel">Suplier note search model</param>
        /// <param name="Suplier">Suplier</param>
        /// <returns>Suplier note list model</returns>
        public virtual SuplierNoteListModel PrepareSuplierNoteListModel(SuplierNoteSearchModel searchModel, Suplier Suplier)
        {
            //if (searchModel == null)
            //    throw new ArgumentNullException(nameof(searchModel));

            //if (Suplier == null)
            //    throw new ArgumentNullException(nameof(Suplier));

            ////get Suplier notes
            //var SuplierNotes = _SuplierService.GetSuplierNotesBySuplier(Suplier.Id, searchModel.Page - 1, searchModel.PageSize);

            ////prepare list model
            //var model = new SuplierNoteListModel().PrepareToGrid(searchModel, SuplierNotes, () =>
            //{
            //    //fill in model values from the entity
            //    return SuplierNotes.Select(note =>
            //    {
            //        //fill in model values from the entity        
            //        var SuplierNoteModel = note.ToModel<SuplierNoteModel>();

            //        //convert dates to the user time
            //        SuplierNoteModel.CreatedOn = _dateTimeHelper.ConvertToUserTime(note.CreatedOnUtc, DateTimeKind.Utc);

            //        //fill in additional values (not existing in the entity)
            //        SuplierNoteModel.Note = _SuplierService.FormatSuplierNoteText(note);

            //        return SuplierNoteModel;
            //    });
            //});

            //return model;
            return new SuplierNoteListModel();
        }

        #endregion
    }
}
