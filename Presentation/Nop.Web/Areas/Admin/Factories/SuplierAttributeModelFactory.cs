﻿using System;
using System.Linq;
using Nop.Core.Domain.Supliers;
using Nop.Services.Localization;
using Nop.Services.Supliers;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Supliers;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Web.Areas.Admin.Factories
{
    public class SuplierAttributeModelFactory : ISuplierAttributeModelFactory
    {
        /// <summary>
        /// Represents the Suplier attribute model factory implementation
        /// </summary>
        #region Fields

        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedModelFactory _localizedModelFactory;
        private readonly ISuplierAttributeService _SuplierAttributeService;

        #endregion

        #region Ctor

        public SuplierAttributeModelFactory(ILocalizationService localizationService,
            ILocalizedModelFactory localizedModelFactory,
            ISuplierAttributeService SuplierAttributeService)
        {
            _localizationService = localizationService;
            _localizedModelFactory = localizedModelFactory;
            _SuplierAttributeService = SuplierAttributeService;
        }

        #endregion

        #region Utilities

        /// <summary>
        /// Prepare Suplier attribute value search model
        /// </summary>
        /// <param name="searchModel">Suplier attribute value search model</param>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        /// <returns>Suplier attribute value search model</returns>
        protected virtual SuplierAttributeValueSearchModel PrepareSuplierAttributeValueSearchModel(SuplierAttributeValueSearchModel searchModel,
            SuplierAttribute SuplierAttribute)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (SuplierAttribute == null)
                throw new ArgumentNullException(nameof(SuplierAttribute));

            searchModel.SuplierAttributeId = SuplierAttribute.Id;

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Prepare Suplier attribute search model
        /// </summary>
        /// <param name="searchModel">Suplier attribute search model</param>
        /// <returns>Suplier attribute search model</returns>
        public virtual SuplierAttributeSearchModel PrepareSuplierAttributeSearchModel(SuplierAttributeSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        /// <summary>
        /// Prepare paged Suplier attribute list model
        /// </summary>
        /// <param name="searchModel">Suplier attribute search model</param>
        /// <returns>Suplier attribute list model</returns>
        public virtual SuplierAttributeListModel PrepareSuplierAttributeListModel(SuplierAttributeSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get Suplier attributes
            var SuplierAttributes = _SuplierAttributeService.GetAllSuplierAttributes().ToPagedList(searchModel);

            //prepare list model
            var model = new SuplierAttributeListModel().PrepareToGrid(searchModel, SuplierAttributes, () =>
            {
                return SuplierAttributes.Select(attribute =>
                {
                        //fill in model values from the entity
                        var attributeModel = attribute.ToModel<SuplierAttributeModel>();

                        //fill in additional values (not existing in the entity)
                        attributeModel.AttributeControlTypeName = _localizationService.GetLocalizedEnum(attribute.AttributeControlType);

                    return attributeModel;
                });
            });

            return model;
        }

        /// <summary>
        /// Prepare Suplier attribute model
        /// </summary>
        /// <param name="model">Suplier attribute model</param>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Suplier attribute model</returns>
        public virtual SuplierAttributeModel PrepareSuplierAttributeModel(SuplierAttributeModel model,
            SuplierAttribute SuplierAttribute, bool excludeProperties = false)
        {
            Action<SuplierAttributeLocalizedModel, int> localizedModelConfiguration = null;

            if (SuplierAttribute != null)
            {
                //fill in model values from the entity
                model ??= SuplierAttribute.ToModel<SuplierAttributeModel>();

                //prepare nested search model
                PrepareSuplierAttributeValueSearchModel(model.SuplierAttributeValueSearchModel, SuplierAttribute);

                //define localized model configuration action
                localizedModelConfiguration = (locale, languageId) =>
                {
                    locale.Name = _localizationService.GetLocalized(SuplierAttribute, entity => entity.Name, languageId, false, false);
                };
            }

            //prepare localized models
            if (!excludeProperties)
                model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            return model;
        }

        /// <summary>
        /// Prepare paged Suplier attribute value list model
        /// </summary>
        /// <param name="searchModel">Suplier attribute value search model</param>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        /// <returns>Suplier attribute value list model</returns>
        public virtual SuplierAttributeValueListModel PrepareSuplierAttributeValueListModel(SuplierAttributeValueSearchModel searchModel,
            SuplierAttribute SuplierAttribute)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (SuplierAttribute == null)
                throw new ArgumentNullException(nameof(SuplierAttribute));

            //get Suplier attribute values
            var SuplierAttributeValues = _SuplierAttributeService.GetSuplierAttributeValues(SuplierAttribute.Id).ToPagedList(searchModel);

            //prepare list model
            var model = new SuplierAttributeValueListModel().PrepareToGrid(searchModel, SuplierAttributeValues, () =>
            {
                    //fill in model values from the entity
                    return SuplierAttributeValues.Select(value => value.ToModel<SuplierAttributeValueModel>());
            });

            return model;
        }

        /// <summary>
        /// Prepare Suplier attribute value model
        /// </summary>
        /// <param name="model">Suplier attribute value model</param>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        /// <param name="SuplierAttributeValue">Suplier attribute value</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Suplier attribute value model</returns>
        public virtual SuplierAttributeValueModel PrepareSuplierAttributeValueModel(SuplierAttributeValueModel model,
            SuplierAttribute SuplierAttribute, SuplierAttributeValue SuplierAttributeValue, bool excludeProperties = false)
        {
            if (SuplierAttribute == null)
                throw new ArgumentNullException(nameof(SuplierAttribute));

            Action<SuplierAttributeValueLocalizedModel, int> localizedModelConfiguration = null;

            if (SuplierAttributeValue != null)
            {
                //fill in model values from the entity
                model ??= SuplierAttributeValue.ToModel<SuplierAttributeValueModel>();

                //define localized model configuration action
                localizedModelConfiguration = (locale, languageId) =>
                {
                    locale.Name = _localizationService.GetLocalized(SuplierAttributeValue, entity => entity.Name, languageId, false, false);
                };
            }

            model.SuplierAttributeId = SuplierAttribute.Id;

            //prepare localized models
            if (!excludeProperties)
                model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

            return model;
        }

        #endregion
    }
}
