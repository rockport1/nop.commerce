﻿using Nop.Core.Domain.Supliers;
using Nop.Web.Areas.Admin.Models.Supliers;

namespace Nop.Web.Areas.Admin.Factories
{
    interface ISuplierAttributeModelFactory
    {
        /// <summary>
        /// Represents the Suplier attribute model factory
        /// </summary>
        public partial interface ISuplierAttributeModelFactory
        {
            /// <summary>
            /// Prepare Suplier attribute search model
            /// </summary>
            /// <param name="searchModel">Suplier attribute search model</param>
            /// <returns>Suplier attribute search model</returns>
            SuplierAttributeSearchModel PrepareSuplierAttributeSearchModel(SuplierAttributeSearchModel searchModel);

            /// <summary>
            /// Prepare paged Suplier attribute list model
            /// </summary>
            /// <param name="searchModel">Suplier attribute search model</param>
            /// <returns>Suplier attribute list model</returns>
            SuplierAttributeListModel PrepareSuplierAttributeListModel(SuplierAttributeSearchModel searchModel);

            /// <summary>
            /// Prepare Suplier attribute model
            /// </summary>
            /// <param name="model">Suplier attribute model</param>
            /// <param name="SuplierAttribute">Suplier attribute</param>
            /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
            /// <returns>Suplier attribute model</returns>
            SuplierAttributeModel PrepareSuplierAttributeModel(SuplierAttributeModel model,
                SuplierAttribute SuplierAttribute, bool excludeProperties = false);

            /// <summary>
            /// Prepare paged Suplier attribute value list model
            /// </summary>
            /// <param name="searchModel">Suplier attribute value search model</param>
            /// <param name="SuplierAttribute">Suplier attribute</param>
            /// <returns>Suplier attribute value list model</returns>
            SuplierAttributeValueListModel PrepareSuplierAttributeValueListModel(SuplierAttributeValueSearchModel searchModel,
                SuplierAttribute SuplierAttribute);

            /// <summary>
            /// Prepare Suplier attribute value model
            /// </summary>
            /// <param name="model">Suplier attribute value model</param>
            /// <param name="SuplierAttribute">Suplier attribute</param>
            /// <param name="SuplierAttributeValue">Suplier attribute value</param>
            /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
            /// <returns>Suplier attribute value model</returns>
            SuplierAttributeValueModel PrepareSuplierAttributeValueModel(SuplierAttributeValueModel model,
                SuplierAttribute SuplierAttribute, SuplierAttributeValue SuplierAttributeValue, bool excludeProperties = false);
        }
    }
}
