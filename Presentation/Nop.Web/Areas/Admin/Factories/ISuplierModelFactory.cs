﻿using Nop.Core.Domain.Supliers;
using Nop.Web.Areas.Admin.Models.Supliers;

namespace Nop.Web.Areas.Admin.Factories
{
    public interface ISuplierModelFactory
    {
        /// <summary> .
        /// Prepare vendor search model
        /// </summary>
        /// <param name="searchModel">Suplier search model</param>
        /// <returns>Suplier search model</returns>
        SuplierSearchModel PrepareSuplierSearchModel(SuplierSearchModel searchModel);

        /// <summary>
        /// Prepare paged vendor list model
        /// </summary>
        /// <param name="searchModel">Suplier search model</param>
        /// <returns>Suplier list model</returns>
        SuplierListModel PrepareSuplierListModel(SuplierSearchModel searchModel);

        /// <summary>
        /// Prepare vendor model
        /// </summary>
        /// <param name="model">Suplier model</param>
        /// <param name="vendor">Suplier</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>Suplier model</returns>
        SuplierModel PrepareSuplierModel(SuplierModel model, Suplier Suplier, bool excludeProperties = false);

        /// <summary>
        /// Prepare paged vendor note list model
        /// </summary>
        /// <param name="searchModel">Suplier note search model</param>
        /// <param name="vendor">Suplier</param>
        /// <returns>Suplier note list model</returns>
        SuplierNoteListModel PrepareSuplierNoteListModel(SuplierNoteSearchModel searchModel, Suplier Suplier);
    }
}
