﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Supliers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Supliers;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Supliers;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Web.Areas.Admin.Controllers
{
    public class SuplierController : BaseAdminController
    {
        #region Fields
        private readonly IAddressService _addressService; 
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ICustomerService _customerService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ISuplierAttributeParser _SuplierAttributeParser;
        private readonly ISuplierAttributeService _SuplierAttributeService;
        private readonly ISuplierModelFactory _SuplierModelFactory;
        private readonly ISuplierService _SuplierService;

        #endregion

        #region Ctor

        public SuplierController(IAddressService addressService,
            ICustomerActivityService customerActivityService,
            ICustomerService customerService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IPictureService pictureService,
            IUrlRecordService urlRecordService,
            ISuplierAttributeParser SuplierAttributeParser,
            ISuplierAttributeService SuplierAttributeService,
            ISuplierModelFactory SuplierModelFactory,
            ISuplierService SuplierService)
        {
            _addressService = addressService;
            _customerActivityService = customerActivityService;
            _customerService = customerService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _localizedEntityService = localizedEntityService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _pictureService = pictureService;
            _urlRecordService = urlRecordService;
            _SuplierAttributeParser = SuplierAttributeParser;
            _SuplierAttributeService = SuplierAttributeService;
            _SuplierModelFactory = SuplierModelFactory;
            _SuplierService = SuplierService;
        }

        #endregion

        #region Utilities

        protected virtual void UpdatePictureSeoNames(Suplier Suplier)
        {
            var picture = _pictureService.GetPictureById(Suplier.PictureId);
            if (picture != null)
                _pictureService.SetSeoFilename(picture.Id, _pictureService.GetPictureSeName(Suplier.Name));
        }

        protected virtual void UpdateLocales(Suplier Suplier, SuplierModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(Suplier,
                    x => x.Name,
                    localized.Name,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(Suplier,
                    x => x.Description,
                    localized.Description,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(Suplier,
                    x => x.MetaKeywords,
                    localized.MetaKeywords,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(Suplier,
                    x => x.MetaDescription,
                    localized.MetaDescription,
                    localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(Suplier,
                    x => x.MetaTitle,
                    localized.MetaTitle,
                    localized.LanguageId);

                //search engine name
                var seName = _urlRecordService.ValidateSeName(Suplier, localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(Suplier, seName, localized.LanguageId);
            }
        }

        protected virtual string ParseSuplierAttributes(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentNullException(nameof(form));

            var attributesXml = string.Empty;
            var SuplierAttributes = _SuplierAttributeService.GetAllSuplierAttributes();
            foreach (var attribute in SuplierAttributes)
            {
                var controlId = $"{NopSuplierDefaults.SuplierAttributePrefix}{attribute.Id}";
                StringValues ctrlAttributes;
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                        ctrlAttributes = form[controlId];
                        if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                        {
                            var selectedAttributeId = int.Parse(ctrlAttributes);
                            if (selectedAttributeId > 0)
                                attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                    attribute, selectedAttributeId.ToString());
                        }

                        break;
                    case AttributeControlType.Checkboxes:
                        var cblAttributes = form[controlId];
                        if (!StringValues.IsNullOrEmpty(cblAttributes))
                        {
                            foreach (var item in cblAttributes.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                            {
                                var selectedAttributeId = int.Parse(item);
                                if (selectedAttributeId > 0)
                                    attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }

                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        //load read-only (already server-side selected) values
                        var attributeValues = _SuplierAttributeService.GetSuplierAttributeValues(attribute.Id);
                        foreach (var selectedAttributeId in attributeValues
                            .Where(v => v.IsPreSelected)
                            .Select(v => v.Id)
                            .ToList())
                        {
                            attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                attribute, selectedAttributeId.ToString());
                        }

                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        ctrlAttributes = form[controlId];
                        if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                        {
                            var enteredText = ctrlAttributes.ToString().Trim();
                            attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                attribute, enteredText);
                        }

                        break;
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.FileUpload:
                    //not supported Suplier attributes
                    default:
                        break;
                }
            }

            return attributesXml;
        }

        #endregion

        #region Supliers

        public virtual IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSupliers))
                return AccessDeniedView();

            //prepare model
            var model = _SuplierModelFactory.PrepareSuplierSearchModel(new SuplierSearchModel());

            return View(model);
        }

        [HttpPost]
        public virtual IActionResult List(SuplierSearchModel searchModel)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSupliers))
                return AccessDeniedDataTablesJson();

            //prepare model
            var model = _SuplierModelFactory.PrepareSuplierListModel(searchModel);

            return Json(model);
        }

        public virtual IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSupliers))
                return AccessDeniedView();

            //prepare model
            var model = _SuplierModelFactory.PrepareSuplierModel(new SuplierModel(), null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public virtual IActionResult Create(SuplierModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSupliers))
                return AccessDeniedView();

            //parse Suplier attributes
            var SuplierAttributesXml = ParseSuplierAttributes(form);
            _SuplierAttributeParser.GetAttributeWarnings(SuplierAttributesXml).ToList()
                .ForEach(warning => ModelState.AddModelError(string.Empty, warning));

            if (ModelState.IsValid)
            {
                var Suplier = model.ToEntity<Suplier>();
                _SuplierService.InsertSuplier(Suplier);

                //activity log
                _customerActivityService.InsertActivity("AddNewSuplier",
                    string.Format(_localizationService.GetResource("ActivityLog.AddNewSuplier"), Suplier.Id), Suplier);

                //search engine name
                model.SeName = _urlRecordService.ValidateSeName(Suplier, model.SeName, Suplier.Name, true);
                _urlRecordService.SaveSlug(Suplier, model.SeName, 0);

                //address
                var address = model.Address.ToEntity<Address>();
                address.CreatedOnUtc = DateTime.UtcNow;

                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _addressService.InsertAddress(address);
                Suplier.AddressId = address.Id;
                _SuplierService.UpdateSuplier(Suplier);

                //Suplier attributes
                _genericAttributeService.SaveAttribute(Suplier, NopSuplierDefaults.SuplierAttributes, SuplierAttributesXml);

                //locales
                UpdateLocales(Suplier, model);

                //update picture seo file name
                UpdatePictureSeoNames(Suplier);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Supliers.Added"));

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = Suplier.Id });
            }

            //prepare model
            model = _SuplierModelFactory.PrepareSuplierModel(model, null, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        public virtual IActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSupliers))
                return AccessDeniedView();

            //try to get a Suplier with the specified id
            var Suplier = _SuplierService.GetSuplierById(id);
            if (Suplier == null || Suplier.Deleted)
                return RedirectToAction("List");

            //prepare model
            var model = _SuplierModelFactory.PrepareSuplierModel(null, Suplier);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual IActionResult Edit(SuplierModel model, bool continueEditing, IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSupliers))
                return AccessDeniedView();

            //try to get a Suplier with the specified id
            var Suplier = _SuplierService.GetSuplierById(model.Id);
            if (Suplier == null || Suplier.Deleted)
                return RedirectToAction("List");

            //parse Suplier attributes
            var SuplierAttributesXml = ParseSuplierAttributes(form);
            _SuplierAttributeParser.GetAttributeWarnings(SuplierAttributesXml).ToList()
                .ForEach(warning => ModelState.AddModelError(string.Empty, warning));

            if (ModelState.IsValid)
            {
                var prevPictureId = Suplier.PictureId;
                Suplier = model.ToEntity(Suplier);
                _SuplierService.UpdateSuplier(Suplier);

                //Suplier attributes
                _genericAttributeService.SaveAttribute(Suplier, NopSuplierDefaults.SuplierAttributes, SuplierAttributesXml);

                //activity log
                _customerActivityService.InsertActivity("EditSuplier",
                    string.Format(_localizationService.GetResource("ActivityLog.EditSuplier"), Suplier.Id), Suplier);

                //search engine name
                model.SeName = _urlRecordService.ValidateSeName(Suplier, model.SeName, Suplier.Name, true);
                _urlRecordService.SaveSlug(Suplier, model.SeName, 0);

                //address
                var address = _addressService.GetAddressById(Suplier.AddressId);
                if (address == null)
                {
                    address = model.Address.ToEntity<Address>();
                    address.CreatedOnUtc = DateTime.UtcNow;

                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.InsertAddress(address);
                    Suplier.AddressId = address.Id;
                    _SuplierService.UpdateSuplier(Suplier);
                }
                else
                {
                    address = model.Address.ToEntity(address);

                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.UpdateAddress(address);
                }

                //locales
                UpdateLocales(Suplier, model);

                //delete an old picture (if deleted or updated)
                if (prevPictureId > 0 && prevPictureId != Suplier.PictureId)
                {
                    var prevPicture = _pictureService.GetPictureById(prevPictureId);
                    if (prevPicture != null)
                        _pictureService.DeletePicture(prevPicture);
                }
                //update picture seo file name
                UpdatePictureSeoNames(Suplier);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Supliers.Updated"));

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = Suplier.Id });
            }

            //prepare model
            model = _SuplierModelFactory.PrepareSuplierModel(model, Suplier, true);

            //if we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        public virtual IActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSupliers))
                return AccessDeniedView();

            //try to get a Suplier with the specified id
            var Suplier = _SuplierService.GetSuplierById(id);
            if (Suplier == null)
                return RedirectToAction("List");

            //clear associated customer references
            var associatedCustomers = _customerService.GetAllCustomers(SuplierId: Suplier.Id);
            foreach (var customer in associatedCustomers)
            {
                customer.SuplierId = 0;
                _customerService.UpdateCustomer(customer);
            }

            //delete a Suplier
            _SuplierService.DeleteSuplier(Suplier);

            //activity log
            _customerActivityService.InsertActivity("DeleteSuplier",
                string.Format(_localizationService.GetResource("ActivityLog.DeleteSuplier"), Suplier.Id), Suplier);

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Supliers.Deleted"));

            return RedirectToAction("List");
        }

        #endregion
    }
}
