﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Supliers
{
    public class ApplySuplierModel : BaseNopModel
    {
        public ApplySuplierModel()
        {
            SuplierAttributes = new List<SuplierAttributeModel>();

        }

        [NopResourceDisplayName("Supliers.ApplyAccount.Name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("Supliers.ApplyAccount.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Supliers.ApplyAccount.Description")]
        public string Description { get; set; }

        public IList<SuplierAttributeModel> SuplierAttributes { get; set; }

        public bool DisplayCaptcha { get; set; }

        public bool TermsOfServiceEnabled { get; set; }
        public bool TermsOfServicePopup { get; set; }

        public bool DisableFormInput { get; set; }
        public string Result { get; set; }
    }
}
