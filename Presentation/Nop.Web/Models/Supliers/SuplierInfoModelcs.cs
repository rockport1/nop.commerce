﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Supliers
{
    public class SuplierInfoModel : BaseNopModel
    {
        public SuplierInfoModel()
        {
            SuplierAttributes = new List<SuplierAttributeModel>(); 
        }

        [NopResourceDisplayName("Account.SuplierInfo.Name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [NopResourceDisplayName("Account.SuplierInfo.Email")]
        public string Email { get; set; }

        [NopResourceDisplayName("Account.SuplierInfo.Description")]
        public string Description { get; set; }

        [NopResourceDisplayName("Account.SuplierInfo.Picture")]
        public string PictureUrl { get; set; }

        public IList<SuplierAttributeModel> SuplierAttributes { get; set; }
    }
}
