﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Supliers;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Seo;
using Nop.Services.Supliers;
using Nop.Web.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Models.Supliers;

namespace Nop.Web.Controllers
{
    public class SuplierController : BasePublicController
    {
        #region Fields

        private readonly CaptchaSettings _captchaSettings;
        private readonly ICustomerService _customerService;
        private readonly IDownloadService _downloadService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ISuplierAttributeParser _SuplierAttributeParser;
        private readonly ISuplierAttributeService _SuplierAttributeService;
        private readonly ISuplierModelFactory _SuplierModelFactory;
        private readonly ISuplierService _SuplierService;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly SuplierSettings _SuplierSettings;

        #endregion

        #region Ctor

        public SuplierController(CaptchaSettings captchaSettings,
            ICustomerService customerService,
            IDownloadService downloadService,
            IGenericAttributeService genericAttributeService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            IUrlRecordService urlRecordService,
            ISuplierAttributeParser SuplierAttributeParser,
            ISuplierAttributeService SuplierAttributeService,
            ISuplierModelFactory SuplierModelFactory,
            ISuplierService SuplierService,
            IWorkContext workContext,
            IWorkflowMessageService workflowMessageService,
            LocalizationSettings localizationSettings,
            SuplierSettings SuplierSettings
            )
        {
            _captchaSettings = captchaSettings; 
            _customerService = customerService;
            _downloadService = downloadService;
            _genericAttributeService = genericAttributeService;
            _localizationService = localizationService;
            _pictureService = pictureService;
            _urlRecordService = urlRecordService;
            _SuplierAttributeParser = SuplierAttributeParser;
            _SuplierAttributeService = SuplierAttributeService;
            _SuplierModelFactory = SuplierModelFactory;
            _SuplierService = SuplierService;
            _workContext = workContext;
            _workflowMessageService = workflowMessageService;
            _localizationSettings = localizationSettings;
            _SuplierSettings = SuplierSettings;
        }

        #endregion

        #region Utilities

        protected virtual void UpdatePictureSeoNames(Suplier Suplier)
        {
            var picture = _pictureService.GetPictureById(Suplier.PictureId);
            if (picture != null)
                _pictureService.SetSeoFilename(picture.Id, _pictureService.GetPictureSeName(Suplier.Name));
        }

        protected virtual string ParseSuplierAttributes(IFormCollection form)
        {
            if (form == null)
                throw new ArgumentNullException(nameof(form));

            var attributesXml = "";
            var attributes = _SuplierAttributeService.GetAllSuplierAttributes();
            foreach (var attribute in attributes)
            {
                var controlId = $"{NopSuplierDefaults.SuplierAttributePrefix}{attribute.Id}";
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                            {
                                var selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var cblAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(cblAttributes))
                            {
                                foreach (var item in cblAttributes.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                )
                                {
                                    var selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //load read-only (already server-side selected) values
                            var attributeValues = _SuplierAttributeService.GetSuplierAttributeValues(attribute.Id);
                            foreach (var selectedAttributeId in attributeValues
                                .Where(v => v.IsPreSelected)
                                .Select(v => v.Id)
                                .ToList())
                            {
                                attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                    attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!StringValues.IsNullOrEmpty(ctrlAttributes))
                            {
                                var enteredText = ctrlAttributes.ToString().Trim();
                                attributesXml = _SuplierAttributeParser.AddSuplierAttribute(attributesXml,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                    case AttributeControlType.ColorSquares:
                    case AttributeControlType.ImageSquares:
                    case AttributeControlType.FileUpload:
                    //not supported Suplier attributes
                    default:
                        break;
                }
            }

            return attributesXml;
        }

        #endregion

        #region Methods

        [HttpsRequirement]
        public virtual IActionResult ApplySuplier()
        {
            if (!_SuplierSettings.AllowCustomersToApplyForSuplierAccount)
                return RedirectToRoute("Homepage");

            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            var model = new ApplySuplierModel();
            model = _SuplierModelFactory.PrepareApplySuplierModel(model, true, false, null);
            return View(model);
        }

        [HttpPost, ActionName("ApplySuplier")]
        [AutoValidateAntiforgeryToken]
        [ValidateCaptcha]
        public virtual IActionResult ApplySuplierSubmit(ApplySuplierModel model, bool captchaValid, IFormFile uploadedFile, IFormCollection form)
        {
            if (!_SuplierSettings.AllowCustomersToApplyForSuplierAccount)
                return RedirectToRoute("Homepage");

            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            if (_customerService.IsAdmin(_workContext.CurrentCustomer))
                ModelState.AddModelError("", _localizationService.GetResource("Supliers.ApplyAccount.IsAdmin"));

            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnApplySuplierPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
            }

            var pictureId = 0;

            if (uploadedFile != null && !string.IsNullOrEmpty(uploadedFile.FileName))
            {
                try
                {
                    var contentType = uploadedFile.ContentType;
                    var SuplierPictureBinary = _downloadService.GetDownloadBits(uploadedFile);
                    var picture = _pictureService.InsertPicture(SuplierPictureBinary, contentType, null);

                    if (picture != null)
                        pictureId = picture.Id;
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", _localizationService.GetResource("Supliers.ApplyAccount.Picture.ErrorMessage"));
                }
            }

            //Suplier attributes
            var SuplierAttributesXml = ParseSuplierAttributes(form);
            _SuplierAttributeParser.GetAttributeWarnings(SuplierAttributesXml).ToList()
                .ForEach(warning => ModelState.AddModelError(string.Empty, warning));

            if (ModelState.IsValid)
            {
                var description = Core.Html.HtmlHelper.FormatText(model.Description, false, false, true, false, false, false);
                //disabled by default
                var Suplier = new Suplier
                {
                    Name = model.Name,
                    Email = model.Email,
                    //some default settings
                    PageSize = 6,
                    AllowCustomersToSelectPageSize = true,
                    PageSizeOptions = _SuplierSettings.DefaultSuplierPageSizeOptions,
                    PictureId = pictureId,
                    Description = description
                };
                _SuplierService.InsertSuplier(Suplier);
                //search engine name (the same as Suplier name)
                var seName = _urlRecordService.ValidateSeName(Suplier, Suplier.Name, Suplier.Name, true);
                _urlRecordService.SaveSlug(Suplier, seName, 0);

                //associate to the current customer
                //but a store owner will have to manually add this customer role to "Supliers" role
                //if he wants to grant access to admin area
                _workContext.CurrentCustomer.SuplierId = Suplier.Id;
                _customerService.UpdateCustomer(_workContext.CurrentCustomer);

                //update picture seo file name
                UpdatePictureSeoNames(Suplier);

                //save Suplier attributes
                _genericAttributeService.SaveAttribute(Suplier, NopSuplierDefaults.SuplierAttributes, SuplierAttributesXml);

                //notify store owner here (email)
                _workflowMessageService.SendNewSuplierAccountApplyStoreOwnerNotification(_workContext.CurrentCustomer,
                    Suplier, _localizationSettings.DefaultAdminLanguageId);

                model.DisableFormInput = true;
                model.Result = _localizationService.GetResource("Supliers.ApplyAccount.Submitted");
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            model = _SuplierModelFactory.PrepareApplySuplierModel(model, false, true, SuplierAttributesXml);
            return View(model);
        }

        [HttpsRequirement]
        public virtual IActionResult Info()
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            if (_workContext.CurrentSuplier == null || !_SuplierSettings.AllowSupliersToEditInfo)
                return RedirectToRoute("CustomerInfo");

            var model = new SuplierInfoModel();
            model = _SuplierModelFactory.PrepareSuplierInfoModel(model, false);
            return View(model);
        }

        [HttpPost, ActionName("Info")]
        [AutoValidateAntiforgeryToken]
        [FormValueRequired("save-info-button")]
        public virtual IActionResult Info(SuplierInfoModel model, IFormFile uploadedFile, IFormCollection form)
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            if (_workContext.CurrentSuplier == null || !_SuplierSettings.AllowSupliersToEditInfo)
                return RedirectToRoute("CustomerInfo");

            Picture picture = null;

            if (uploadedFile != null && !string.IsNullOrEmpty(uploadedFile.FileName))
            {
                try
                {
                    var contentType = uploadedFile.ContentType;
                    var SuplierPictureBinary = _downloadService.GetDownloadBits(uploadedFile);
                    picture = _pictureService.InsertPicture(SuplierPictureBinary, contentType, null);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", _localizationService.GetResource("Account.SuplierInfo.Picture.ErrorMessage"));
                }
            }

            var Suplier = _workContext.CurrentSuplier;
            var prevPicture = _pictureService.GetPictureById(Suplier.PictureId);

            //Suplier attributes
            var SuplierAttributesXml = ParseSuplierAttributes(form);
            _SuplierAttributeParser.GetAttributeWarnings(SuplierAttributesXml).ToList()
                .ForEach(warning => ModelState.AddModelError(string.Empty, warning));

            if (ModelState.IsValid)
            {
                var description = Core.Html.HtmlHelper.FormatText(model.Description, false, false, true, false, false, false);

                Suplier.Name = model.Name;
                Suplier.Email = model.Email;
                Suplier.Description = description;

                if (picture != null)
                {
                    Suplier.PictureId = picture.Id;

                    if (prevPicture != null)
                        _pictureService.DeletePicture(prevPicture);
                }

                //update picture seo file name
                UpdatePictureSeoNames(Suplier);

                _SuplierService.UpdateSuplier(Suplier);

                //save Suplier attributes
                _genericAttributeService.SaveAttribute(Suplier, NopSuplierDefaults.SuplierAttributes, SuplierAttributesXml);

                //notifications
                if (_SuplierSettings.NotifyStoreOwnerAboutSuplierInformationChange)
                    _workflowMessageService.SendSuplierInformationChangeNotification(Suplier, _localizationSettings.DefaultAdminLanguageId);

                return RedirectToAction("Info");
            }

            //If we got this far, something failed, redisplay form
            model = _SuplierModelFactory.PrepareSuplierInfoModel(model, true, SuplierAttributesXml);
            return View(model);
        }

        [HttpPost, ActionName("Info")]
        [AutoValidateAntiforgeryToken]
        [FormValueRequired("remove-picture")]
        public virtual IActionResult RemovePicture()
        {
            if (!_customerService.IsRegistered(_workContext.CurrentCustomer))
                return Challenge();

            if (_workContext.CurrentSuplier == null || !_SuplierSettings.AllowSupliersToEditInfo)
                return RedirectToRoute("CustomerInfo");

            var Suplier = _workContext.CurrentSuplier;
            var picture = _pictureService.GetPictureById(Suplier.PictureId);

            if (picture != null)
                _pictureService.DeletePicture(picture);

            Suplier.PictureId = 0;
            _SuplierService.UpdateSuplier(Suplier);

            //notifications
            if (_SuplierSettings.NotifyStoreOwnerAboutSuplierInformationChange)
                _workflowMessageService.SendSuplierInformationChangeNotification(Suplier, _localizationSettings.DefaultAdminLanguageId);

            return RedirectToAction("Info");
        }

        #endregion
    }
}
