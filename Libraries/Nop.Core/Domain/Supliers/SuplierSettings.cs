﻿using Nop.Core.Configuration;

namespace Nop.Core.Domain.Supliers
{
    public class SuplierSettings : ISettings
    {
        /// <summary> .
        /// Gets or sets the default value to use for Suplier page size options (for new Supliers)
        /// </summary>
        public string DefaultSuplierPageSizeOptions { get; set; }

        /// <summary>
        /// Gets or sets the value indicating how many Supliers to display in Supliers block
        /// </summary>
        public int SupliersBlockItemsToDisplay { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to display Suplier name on the product details page
        /// </summary>
        public bool ShowSuplierOnProductDetailsPage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to display Suplier name on the order details page
        /// </summary>
        public bool ShowSuplierOnOrderDetailsPage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether customers can contact Supliers
        /// </summary>
        public bool AllowCustomersToContactSupliers { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether users can fill a form to become a new Suplier
        /// </summary>
        public bool AllowCustomersToApplyForSuplierAccount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Supliers have to accept terms of service during registration
        /// </summary>
        public bool TermsOfServiceEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether it is possible to carry out advanced search in the store by Suplier
        /// </summary>
        public bool AllowSearchBySuplier { get; set; }

        /// <summary>
        /// Get or sets a value indicating whether Suplier can edit information about itself (public store)
        /// </summary>
        public bool AllowSupliersToEditInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the store owner is notified that the Suplier information has been changed
        /// </summary>
        public bool NotifyStoreOwnerAboutSuplierInformationChange { get; set; }

        /// <summary>
        /// Gets or sets a maximum number of products per Suplier
        /// </summary>
        public int MaximumProductNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Supliers are allowed to import products
        /// </summary>
        public bool AllowSupliersToImportProducts { get; set; }
    }
}
