﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Supliers;

namespace Nop.Data.Mapping.Builders.Supliers
{
    public class SuplierBuilder : NopEntityBuilder<Suplier>
    {
        #region Methods

        /// <summary> .
        /// Apply entity configuration
        /// </summary>
        /// <param name="table">Create table expression builder</param>
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(Suplier.Name)).AsString(400).NotNullable()
                .WithColumn(nameof(Suplier.Email)).AsString(400).Nullable()
                .WithColumn(nameof(Suplier.MetaKeywords)).AsString(400).Nullable()
                .WithColumn(nameof(Suplier.MetaTitle)).AsString(400).Nullable()
                .WithColumn(nameof(Suplier.PageSizeOptions)).AsString(200).Nullable();
        }

        #endregion
    }
}
