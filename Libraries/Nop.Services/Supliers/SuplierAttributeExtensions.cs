﻿using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Supliers;

namespace Nop.Services.Supliers
{
    public static class SuplierAttributeExtensions
    {
        /// <summary> .
        /// A value indicating whether this Suplier attribute should have values
        /// </summary>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        /// <returns>True if the attribute should have values; otherwise false</returns>
        public static bool ShouldHaveValues(this SuplierAttribute SuplierAttribute)
        {
            if (SuplierAttribute == null)
                return false;

            if (SuplierAttribute.AttributeControlType == AttributeControlType.TextBox ||
                SuplierAttribute.AttributeControlType == AttributeControlType.MultilineTextbox ||
                SuplierAttribute.AttributeControlType == AttributeControlType.Datepicker ||
                SuplierAttribute.AttributeControlType == AttributeControlType.FileUpload)
                return false;

            //other attribute control types support values
            return true;
        }
    }
}
