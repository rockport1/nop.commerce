﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
//using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Supliers;
using Nop.Core.Html;
using Nop.Data;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;

namespace Nop.Services.Supliers
{
    public class SuplierService : ISuplierService
    {
        /// <summary> .
        /// Suplier service
        /// </summary>
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        //private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Suplier> _SuplierRepository;
        //private readonly IRepository<SuplierNote> _SuplierNoteRepository;

        #endregion

        #region Ctor

        public SuplierService(IEventPublisher eventPublisher,
            //IRepository<Product> productRepository,
            IRepository<Suplier> SuplierRepository)
        //IRepository<SuplierNote> SuplierNoteRepository)
        {
            _eventPublisher = eventPublisher;
            //_productRepository = productRepository;
            _SuplierRepository = SuplierRepository;
            //_SuplierNoteRepository = SuplierNoteRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a Suplier by Suplier identifier
        /// </summary>
        /// <param name="SuplierId">Suplier identifier</param>
        /// <returns>Suplier</returns>
        public virtual Suplier GetSuplierById(int SuplierId)
        {
            if (SuplierId == 0)
                return null;

            return _SuplierRepository.ToCachedGetById(SuplierId);
        }

        /// <summary>
        /// Gets a Suplier by product identifier
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>Suplier</returns>
        /// 

        //******************** Not Related to Product ********************
        //public virtual Suplier GetSuplierByProductId(int productId)
        //{
        //    if (productId == 0)
        //        return null;

        //    return (from v in _SuplierRepository.Table
        //            join p in _productRepository.Table on v.Id equals p.SuplierId
        //            select v).FirstOrDefault();
        //}

        /// <summary>
        /// Gets a Supliers by product identifiers
        /// </summary>
        /// <param name="productIds">Array of product identifiers</param>
        /// <returns>Supliers</returns>
        //public virtual IList<Suplier> GetSupliersByProductIds(int[] productIds)
        //{
        //    if (productIds is null)
        //        throw new ArgumentNullException(nameof(productIds));

        //    return (from v in _SuplierRepository.Table
        //            join p in _productRepository.Table on v.Id equals p.SuplierId
        //            where productIds.Contains(p.Id) && !v.Deleted && v.Active
        //            group v by p.Id into v
        //            select v.First()).ToList();
        //}

        /// <summary>
        /// Delete a Suplier
        /// </summary>
        /// <param name="Suplier">Suplier</param>
        public virtual void DeleteSuplier(Suplier Suplier)
        {
            if (Suplier == null)
                throw new ArgumentNullException(nameof(Suplier));

            Suplier.Deleted = true;
            UpdateSuplier(Suplier);

            //event notification
            _eventPublisher.EntityDeleted(Suplier);
        }

        /// <summary>
        /// Gets all Supliers
        /// </summary>
        /// <param name="name">Suplier name</param>
        /// <param name="email">Suplier email</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Supliers</returns>
        public virtual IPagedList<Suplier> GetAllSupliers(string name = "", string email = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _SuplierRepository.Table;
            if (!string.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.Name.Contains(name));

            if (!string.IsNullOrWhiteSpace(email))
                query = query.Where(v => v.Email.Contains(email));

            if (!showHidden)
                query = query.Where(v => v.Active);

            query = query.Where(v => !v.Deleted);
            query = query.OrderBy(v => v.DisplayOrder).ThenBy(v => v.Name).ThenBy(v => v.Email);

            var Supliers = new PagedList<Suplier>(query, pageIndex, pageSize);
            return Supliers;
        }

        /// <summary>
        /// Gets Supliers
        /// </summary>
        /// <param name="SuplierIds">Suplier identifiers</param>
        /// <returns>Supliers</returns>
        public virtual IList<Suplier> GetSupliersByIds(int[] SuplierIds)
        {
            var query = _SuplierRepository.Table;
            if (SuplierIds != null)
                query = query.Where(v => SuplierIds.Contains(v.Id));

            return query.ToList();
        }

        /// <summary>
        /// Inserts a Suplier
        /// </summary>
        /// <param name="Suplier">Suplier</param>
        public virtual void InsertSuplier(Suplier Suplier)
        {
            if (Suplier == null)
                throw new ArgumentNullException(nameof(Suplier));

            _SuplierRepository.Insert(Suplier);

            //event notification
            _eventPublisher.EntityInserted(Suplier);
        }

        /// <summary>
        /// Updates the Suplier
        /// </summary>
        /// <param name="Suplier">Suplier</param>
        public virtual void UpdateSuplier(Suplier Suplier)
        {
            if (Suplier == null)
                throw new ArgumentNullException(nameof(Suplier));

            _SuplierRepository.Update(Suplier);

            //event notification
            _eventPublisher.EntityUpdated(Suplier);
        }

        //***************** Not Implemented *******************

        ///// <summary>
        ///// Gets a vendor note
        ///// </summary>
        ///// <param name="vendorNoteId">The vendor note identifier</param>
        ///// <returns>Vendor note</returns>
        //public virtual VendorNote GetVendorNoteById(int vendorNoteId)
        //{
        //    if (vendorNoteId == 0)
        //        return null;

        //    return _vendorNoteRepository.ToCachedGetById(vendorNoteId);
        //}

        ///// <summary>
        ///// Gets all vendor notes
        ///// </summary>
        ///// <param name="vendorId">Vendor identifier</param>
        ///// <param name="pageIndex">Page index</param>
        ///// <param name="pageSize">Page size</param>
        ///// <returns>Vendor notes</returns>
        //public virtual IPagedList<VendorNote> GetVendorNotesByVendor(int vendorId, int pageIndex = 0, int pageSize = int.MaxValue)
        //{
        //    var query = _vendorNoteRepository.Table.Where(vn => vn.VendorId == vendorId);

        //    query = query.OrderBy(v => v.CreatedOnUtc).ThenBy(v => v.Id);

        //    return new PagedList<VendorNote>(query, pageIndex, pageSize);
        //}

        ///// <summary>
        ///// Deletes a vendor note
        ///// </summary>
        ///// <param name="vendorNote">The vendor note</param>
        //public virtual void DeleteVendorNote(VendorNote vendorNote)
        //{
        //    if (vendorNote == null)
        //        throw new ArgumentNullException(nameof(vendorNote));

        //    _vendorNoteRepository.Delete(vendorNote);

        //    //event notification
        //    _eventPublisher.EntityDeleted(vendorNote);
        //}

        ///// <summary>
        ///// Inserts a vendor note
        ///// </summary>
        ///// <param name="vendorNote">Vendor note</param>
        //public virtual void InsertVendorNote(VendorNote vendorNote)
        //{
        //    if (vendorNote == null)
        //        throw new ArgumentNullException(nameof(vendorNote));

        //    _vendorNoteRepository.Insert(vendorNote);

        //    //event notification
        //    _eventPublisher.EntityInserted(vendorNote);
        //}

        ///// <summary>
        ///// Formats the vendor note text
        ///// </summary>
        ///// <param name="vendorNote">Vendor note</param>
        ///// <returns>Formatted text</returns>
        //public virtual string FormatVendorNoteText(VendorNote vendorNote)
        //{
        //    if (vendorNote == null)
        //        throw new ArgumentNullException(nameof(vendorNote));

        //    var text = vendorNote.Note;

        //    if (string.IsNullOrEmpty(text))
        //        return string.Empty;

        //    text = HtmlHelper.FormatText(text, false, true, false, false, false, false);

        //    return text;
        //}

        #endregion
    }

}
