﻿using System.Collections.Generic;
using Nop.Core.Domain.Supliers;

namespace Nop.Services.Supliers
{
    public interface ISuplierAttributeParser
    {
        /// <summary> .
        /// Gets Suplier attributes from XML
        /// </summary>
        /// <param name="attributesXml">Attributes in XML format</param>
        /// <returns>List of Suplier attributes</returns>
        IList<SuplierAttribute> ParseSuplierAttributes(string attributesXml);

        /// <summary>
        /// Get Suplier attribute values from XML
        /// </summary>
        /// <param name="attributesXml">Attributes in XML format</param>
        /// <returns>List of Suplier attribute values</returns>
        IList<SuplierAttributeValue> ParseSuplierAttributeValues(string attributesXml);

        /// <summary>
        /// Gets values of the selected Suplier attribute
        /// </summary>
        /// <param name="attributesXml">Attributes in XML format</param>
        /// <param name="SuplierAttributeId">Suplier attribute identifier</param>
        /// <returns>Values of the Suplier attribute</returns>
        IList<string> ParseValues(string attributesXml, int SuplierAttributeId);

        /// <summary>
        /// Adds a Suplier attribute
        /// </summary>
        /// <param name="attributesXml">Attributes in XML format</param>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        /// <param name="value">Value</param>
        /// <returns>Attributes in XML format</returns>
        string AddSuplierAttribute(string attributesXml, SuplierAttribute SuplierAttribute, string value);

        /// <summary>
        /// Validates Suplier attributes
        /// </summary>
        /// <param name="attributesXml">Attributes in XML format</param>
        /// <returns>Warnings</returns>
        IList<string> GetAttributeWarnings(string attributesXml);
    }
}
