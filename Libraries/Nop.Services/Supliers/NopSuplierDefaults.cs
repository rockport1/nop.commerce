﻿using Nop.Core.Caching;

namespace Nop.Services.Supliers
{
    public class NopSuplierDefaults
    {
        /// <summary> . 
        /// Gets a generic attribute key to store Suplier additional info
        /// </summary>
        public static string SuplierAttributes => "SuplierAttributes";

        /// <summary>
        /// Gets default prefix for Suplier
        /// </summary>
        public static string SuplierAttributePrefix => "Suplier_attribute_";

        #region Caching defaults

        /// <summary>
        /// Gets a key for caching all Suplier attributes
        /// </summary>
        public static CacheKey SuplierAttributesAllCacheKey => new CacheKey("Nop.Suplierattribute.all");

        /// <summary>
        /// Gets a key for caching Suplier attribute values of the Suplier attribute
        /// </summary>
        /// <remarks>
        /// {0} : Suplier attribute ID
        /// </remarks>
        public static CacheKey SuplierAttributeValuesAllCacheKey => new CacheKey("Nop.Suplierattributevalue.all-{0}");

        #endregion
    }
}
