﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
//using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Supliers;
using Nop.Core.Html;
using Nop.Data;
using Nop.Services.Caching.Extensions;
using Nop.Services.Events;

namespace Nop.Services.Supliers
{
    public interface ISuplierService
    {
        /// <summary> .
        /// Gets a Suplier by Suplier identifier
        /// </summary>
        /// <param name="SuplierId">Vendor identifier</param>
        /// <returns>Suplier</returns>
        Suplier GetSuplierById(int SuplierId);

        /// <summary>
        /// Gets a Supliers by product identifiers
        /// </summary>
        /// <param name="productIds">Array of product identifiers</param>
        /// <returns>Supliers</returns>
        //IList<Suplier> GetSupliersByProductIds(int[] productIds);

        /// <summary>
        /// Gets a Suplier by product identifier
        /// </summary>
        /// <param name="productId">Product identifier</param>
        /// <returns>Suplier</returns>
        //Suplier GetSuplierByProductId(int productId);

        /// <summary>
        /// Delete a Suplier
        /// </summary>
        /// <param name="Suplier">Suplier</param>
        void DeleteSuplier(Suplier Suplier);

        /// <summary>
        /// Gets all Supliers
        /// </summary>
        /// <param name="name">Suplier name</param>
        /// <param name="email">Suplier email</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Supliers</returns>
        IPagedList<Suplier> GetAllSupliers(string name = "", string email = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        /// Gets Supliers
        /// </summary>
        /// <param name="SuplierIds">Suplier identifiers</param>
        /// <returns>Supliers</returns>
        IList<Suplier> GetSupliersByIds(int[] SuplierIds);

        /// <summary>
        /// Inserts a Suplier
        /// </summary>
        /// <param name="Suplier">Suplier</param>
        void InsertSuplier(Suplier Suplier);

        /// <summary>
        /// Updates the Suplier
        /// </summary>
        /// <param name="Suplier">Suplier</param>
        void UpdateSuplier(Suplier Suplier);

        /// <summary>
        /// Gets a Suplier note
        /// </summary>
        /// <param name="SuplierNoteId">The Suplier note identifier</param>
        /// <returns>Suplier note</returns>
        /// 

        //**********************Not Implemented *********************

        //VendorNote GetVendorNoteById(int vendorNoteId);

        ///// <summary>
        ///// Gets all vendor notes
        ///// </summary>
        ///// <param name="vendorId">Vendor identifier</param>
        ///// <param name="pageIndex">Page index</param>
        ///// <param name="pageSize">Page size</param>
        ///// <returns>Vendor notes</returns>
        //IPagedList<VendorNote> GetVendorNotesByVendor(int vendorId, int pageIndex = 0, int pageSize = int.MaxValue);

        ///// <summary>
        ///// Deletes a vendor note
        ///// </summary>
        ///// <param name="vendorNote">The vendor note</param>
        //void DeleteVendorNote(VendorNote vendorNote);

        ///// <summary>
        ///// Inserts a vendor note
        ///// </summary>
        ///// <param name="vendorNote">Vendor note</param>
        //void InsertVendorNote(VendorNote vendorNote);

        ///// <summary>
        ///// Formats the vendor note text
        ///// </summary>
        ///// <param name="vendorNote">Vendor note</param>
        ///// <returns>Formatted text</returns>
        //string FormatVendorNoteText(VendorNote vendorNote);
    }
}
