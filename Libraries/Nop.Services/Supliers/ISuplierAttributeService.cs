﻿using System.Collections.Generic;
using Nop.Core.Domain.Supliers;

namespace Nop.Services.Supliers
{
    public interface ISuplierAttributeService
    {
        #region Suplier attributes

        /// <summary> .
        /// Gets all Suplier attributes
        /// </summary>
        /// <returns>Suplier attributes</returns>
        IList<SuplierAttribute> GetAllSuplierAttributes();

        /// <summary>
        /// Gets a Suplier attribute 
        /// </summary>
        /// <param name="SuplierAttributeId">Suplier attribute identifier</param>
        /// <returns>Suplier attribute</returns>
        SuplierAttribute GetSuplierAttributeById(int SuplierAttributeId);

        /// <summary>
        /// Inserts a Suplier attribute
        /// </summary>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        void InsertSuplierAttribute(SuplierAttribute SuplierAttribute);

        /// <summary>
        /// Updates a Suplier attribute
        /// </summary>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        void UpdateSuplierAttribute(SuplierAttribute SuplierAttribute);

        /// <summary>
        /// Deletes a Suplier attribute
        /// </summary>
        /// <param name="SuplierAttribute">Suplier attribute</param>
        void DeleteSuplierAttribute(SuplierAttribute SuplierAttribute);

        #endregion

        #region Suplier attribute values

        /// <summary>
        /// Gets Suplier attribute values by Suplier attribute identifier
        /// </summary>
        /// <param name="SuplierAttributeId">The Suplier attribute identifier</param>
        /// <returns>Suplier attribute values</returns>
        IList<SuplierAttributeValue> GetSuplierAttributeValues(int SuplierAttributeId);

        /// <summary>
        /// Gets a Suplier attribute value
        /// </summary>
        /// <param name="SuplierAttributeValueId">Suplier attribute value identifier</param>
        /// <returns>Suplier attribute value</returns>
        SuplierAttributeValue GetSuplierAttributeValueById(int SuplierAttributeValueId);

        /// <summary>
        /// Inserts a Suplier attribute value
        /// </summary>
        /// <param name="SuplierAttributeValue">Suplier attribute value</param>
        void InsertSuplierAttributeValue(SuplierAttributeValue SuplierAttributeValue);

        /// <summary>
        /// Updates a Suplier attribute value
        /// </summary>
        /// <param name="SuplierAttributeValue">Suplier attribute value</param>
        void UpdateSuplierAttributeValue(SuplierAttributeValue SuplierAttributeValue);

        /// <summary>
        /// Deletes a Suplier attribute value
        /// </summary>
        /// <param name="SuplierAttributeValue">Suplier attribute value</param>
        void DeleteSuplierAttributeValue(SuplierAttributeValue SuplierAttributeValue);

        #endregion
    }
}
